from os import path
import sys
sys.path.insert(0, path.abspath(path.join(path.dirname(__file__), '../')))
import lib.links.links as links

import pvaccess
import time
import json
from typing import Dict, Union
from PVGenerator import NtTableGenerator, NtScalarGenerator, scalarGenerator, get_pv_generator, set_pv_generator, arrayGenerator, structGenerator


class PVAServer(object):
    def __init__(self):
        while True:
            try:
                self.server = pvaccess.PvaServer()
                break
            except RuntimeError as err:
                print(f"Failed to create pvAccess server with error message: {err}")
                print('Retrying in 10s')
                time.sleep(10)

        self.pv_generator_map: Dict[str, Union[NtTableGenerator,NtScalarGenerator, arrayGenerator, structGenerator]] = {}


    def add_pv(self, lnks, write_to_topic_func):
        """
        Creates a PvObject for get_reference and set_reference if defined in the link. 1 PV is configured for each.
        The PV objects are then added to the pvaccess server as a record with a PV name. The object is also added to
        the link and can be accessed for update.
        Now NTTypes are used and it is not essential to add that to the PV (as before ts = pvaccess.PvTimeStamp(1, 1))
        :param lnks:
        :param write_to_topic_func:
        :return:
        """
        for lnk in lnks:
            print("In add PV for:  {}".format(lnk))

            pv_name = lnk.get_reference
            mqtt_topic = lnk.value_topic
            new_pv_generator = get_pv_generator(lnk)
            new_pv = new_pv_generator.get_empty_pv()
            self.pv_generator_map[mqtt_topic] = new_pv_generator
            self.server.addRecord(pv_name, new_pv)  # TODO . NOW removed with iIF lower down...


            #  Adding the created pvObject to the Link object (this allows updating in callback)

            # if lnk.value_topic not in self.pv_generator_map: #  TODO filtering out JN stuff. Shoudl now be removed
            #     print("value topic not in pv_genarator map-----------------shoudl not happen --------")
            #     #lnk.get_reference_pv_obj = get_pv

            #  TODO below filtering out PVs that is not setup with new JN approach
            #  TODO: hasattribute check below is temp fix for NTtables. Currently att links get set PV... wjhic makes issues..
            if lnk.set_reference:# and  hasattr(value_complete, 'pvname') :

                #  Creating a new PV object for the set_reference
                #  TODO: Unfortunately currently not solved how to use NTtypes! The problem is when a the callback
                #        function is triggered by an update ONLY the PVobject is recieved in the callback, an dcurrently
                #        I have not found a way to extract from which PV that originates!! So teh corresponding link can
                #        not be found! Temporary solution is to use non NTtype PVs and pass a "name"

                #  TODO Trying here to copy the get PV defined above. However, need to rethink on handlinonly Set PVs..
                #set_pv = get_pv.copy()  # pvaccess.PvObject(value_complete)

                pv_name = lnk.set_reference
                mqtt_topic = lnk.set_topic
                new_pv_generator = set_pv_generator(lnk)  #   OBS for SET PV. Needed separate to add pvname
                new_pv = new_pv_generator.get_empty_pv()
                self.pv_generator_map[mqtt_topic] = new_pv_generator
                #self.server.addRecord(pv_name, new_pv)  #
                self.server.addRecord(pv_name, new_pv, write_to_topic_func)
                print("Just confirming that we added a set pv for this-----------------")
                #lnk.set_reference_pv_obj = set_pv  #  TODO Does this need to be added in new solution?

            else:
                print("No set reference provided in this link. No set PV created")

            if lnk.set_reference:
                print("Adding PV: set reference ->  {}  set topic->  {}".format(lnk.set_reference, lnk.set_topic))

            if lnk.get_reference:
                print("Adding PV: get reference ->  {}  get topic->  {}".format(lnk.get_reference, lnk.get_topic))

        return lnks  # Returns a link list of all listed links on this "server"

    def remove_pv(self, lnk):
        if lnk.set_reference:
            if self.server.hasRecord(lnk.set_reference):
                self.server.removeRecord(lnk.set_reference)
        if lnk.get_reference:
            if self.server.hasRecord(lnk.get_reference):
                self.server.removeRecord(lnk.get_reference)
                self.pv_generator_map.pop(lnk.value_topic)  # TODO: Verify usage value/get topic in app_pv and here


    def write_to_pv(self, client, userdata, msg):
        """
            This function is triggered as callback function when a link topic (e.g. from HW side)have been updated.
            The corresponding PV is fetched from the linklist by the messages value_topic (that was updated).
            The data in the msg should follow SECoP datainfo format [data, {"t":timestamp}, ....]

            TODO: Need to add syntax checks and also handling of more types.
            """
        message = json.loads(msg.payload.decode('utf-8'))
        print("Incoming message on: {}".format( msg.topic))
        #  TODO NEw stuff from JN for scalars. Shoudl just catch those types using JNs new stuff
        if msg.topic in self.pv_generator_map:
            generator = self.pv_generator_map[msg.topic]
            self.server.update(generator.pv_name, generator.generate_pv_from_data(message))


        else:  # TODO all old code!
            print(" Should not come here to write anymore................")

