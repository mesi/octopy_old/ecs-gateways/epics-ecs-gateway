import pvaccess
import numpy as np
import time
from lib.secop.datainfo import *
import collections


def get_epics_type(type_string):
    #  For the Enum type, which does not seem to be included fully as a type..in Pvapy
    #  it is instead added as the type of the "Value" of the enum NT PV type Enum...
    ntenum = pvaccess.NtEnum()

    type_map = {
        SecInt: pvaccess.LONG,
        SecDouble: pvaccess.DOUBLE,
        SecString: pvaccess.STRING,
        SecBool: pvaccess.BOOLEAN,#.INT,
        SecEnum: type(ntenum.getValue())#pvaccess.INT,
    }
    try:
        return type_map[type_string]
    except KeyError:
        return pvaccess.STRING


def get_python_type(type_string):
    type_map = {
        SecInt: int,
        SecDouble: float,
        SecString: str,
        SecBool: bool,  # int,
        SecEnum: int  # TODO. What woudl be ccorrect here ..?
    }
    try:
        return type_map[type_string]
    except KeyError:
        return str


SECOP_TO_EPICS_TYPES = {
    SecDouble: pvaccess.DOUBLE,
    SecInt: pvaccess.LONG,
    SecBool: pvaccess.BOOLEAN,
    SecString: pvaccess.STRING
}


def set_description(pv, description):
    if description is not None:
        pv.setDescriptor(description)


def extract_pv_metadata(link):  #  TODO. Only works for NT objects!!
    applicators = []
    if hasattr(link, "description"):
        # applicators.append(lambda pv: set_description(pv, link.description))
        applicators.append(lambda pv: pv.getDisplay().setDescription(link.description))
    if hasattr(link.datainfo, 'unit') and link.datainfo.unit:
        applicators.append(lambda pv: pv.getDisplay().setUnits(link.datainfo.unit))
    if hasattr(link.datainfo, 'min') and link.datainfo.min:
        applicators.append(lambda pv: pv.getControl().setLimitLow(float(link.datainfo.min)))
    if hasattr(link.datainfo, 'max') and link.datainfo.max:
        applicators.append(lambda pv: pv.getControl().setLimitHigh(float(link.datainfo.max)))

    return applicators


def apply_pv_metadata2(link,pv):  # TODO. Only works for NT objects!!
    if hasattr(link, "description"):
        pv.set({"display.description": link.description})
    if hasattr(link.datainfo, 'unit') and link.datainfo.unit:
        pv.set({"display.units": link.datainfo.unit})
    if hasattr(link.datainfo, 'min') and link.datainfo.min:
        pv.set({"control.limitLow": link.datainfo.min})
    if hasattr(link.datainfo, 'max') and link.datainfo.max:
        pv.set({"control.limitHigh": link.datainfo.max})

def apply_pv_metadata(applicators, pv):
    for applicator in applicators:
        try:
            applicator(pv)
        except AttributeError:
            pass  # Unable to apply meta data
    return pv


def apply_NTpv_timestamp(pv, unix_nanoseconds):  #  TODO Only NT types have tsetTimeStamp function!
    timestamp = pvaccess.PvTimeStamp(unix_nanoseconds // 1000000000,
                                     (unix_nanoseconds - (unix_nanoseconds // 1000000000 * 1000000000)))
    pv.setTimeStamp(timestamp)
    return pv


def apply_pv_timestamp(pv, unix_nanoseconds):
    timestamp = pvaccess.PvTimeStamp(unix_nanoseconds // 1000000000,
                                     (unix_nanoseconds - (unix_nanoseconds // 1000000000 * 1000000000)))
    pv['timeStamp'] = timestamp
    return pv

def generate_timestamp(data):
    if len(data) > 1:
        if "t" in data[1]:  # TODO: Add this fix to the other generators
            unix_nanoseconds = data[1]["t"]
        else:
            print("no t, applying local time")
            unix_nanoseconds = round(time.time() * 1000000000)
    else:
        print("no t, applying local time")
        unix_nanoseconds = round(time.time() * 1000000000)
    return unix_nanoseconds

def apply_alarm_to_pv(pv, meta_data_message):
    if "alarm" in meta_data_message.keys():
        pv.getAlarm().setMessage(meta_data_message["alarm"])


def exract_dict_from_struct(dict):
    # At this point it is clear the incoming dict is a struct!
    dictatsionefinale = {}
    for k, member in dict.items():
        if member["type"] == "struct":
            dictatsionefinale[k] = exract_dict_from_struct(member["members"])
        elif member["type"] == "array":
            if member["members"]["type"] == "struct" or member["members"]["type"] == "array":
                dictatsionefinale[k] = [exract_dict_from_struct(member["members"])]
            else:
                dictatsionefinale[k] = [get_epics_type(member["members"]["type"])]

        else:
            dictatsionefinale[k] = get_epics_type(member["type"])

    struct_dict = dictatsionefinale

    return struct_dict


class NtScalarGenerator:  # Only used for readonly. (as a set PV need a name currently)
    def __init__(self, link):
        self.epics_type = get_epics_type(link.datainfo.__class__)
        self.python_type = get_python_type(link.datainfo.__class__)
        self.meta_data_applicators = extract_pv_metadata(link)
        self.pv_name = link.get_reference

    def get_empty_pv(self):
        return_value = pvaccess.NtScalar(self.epics_type)
        apply_pv_metadata(self.meta_data_applicators, return_value)
        return return_value

    def generate_pv_from_data(self, data):
        return_value = self.get_empty_pv()
        return_value.setValue(self.python_type(data[0]))
        unix_nanoseconds = generate_timestamp(data)
        if unix_nanoseconds < 2000000000: # Temp fix for when timestamp is in seconds and not ns
            apply_NTpv_timestamp(return_value, int(unix_nanoseconds * 1e9)) #  TODO this is a special case.. for the 1252 latched ts time only..
        else:
            apply_NTpv_timestamp(return_value, int(unix_nanoseconds))

        apply_alarm_to_pv(return_value, data[1])
        return return_value


class scalarGenerator:  # The NT type cannot add pvname field required for set PV
    def __init__(self, link):
        self.link = link
        self.epics_type = get_epics_type(link.datainfo.__class__)
        self.python_type = get_python_type(link.datainfo.__class__)
        self.meta_data_applicators = extract_pv_metadata(link)
        self.pv_name = link.get_reference
        self.pvname = link.set_reference  # For identifying set PV in write to topic
        print("epics type:  {}   pvname:  {}".format(self.epics_type, self.pvname))

    def get_empty_pv(self):
        # return_value = pvaccess.NtScalar(self.epics_type)
        # apply_pv_metadata(self.meta_data_applicators, return_value)  # TODO consider if this os a version can be used
        timestamp = pvaccess.PvTimeStamp(1, 1)  # Creating a pvapy timestamp structure to define the type in the PVobject
        alarm = pvaccess.PvAlarm(0, 0, "")  # Creating a pvapy alarm structure to define the type in the PVobject
        pv_conf = {
            'value': self.epics_type,
            'descriptor': pvaccess.STRING,
            'timeStamp': timestamp,
            'control': pvaccess.PvControl(),
            'display': pvaccess.PvDisplay(),
            'alarm': alarm,
            "pvname": pvaccess.STRING
        }
        get_pv = pvaccess.PvObject(pv_conf)
        apply_pv_metadata2( self.link, get_pv)
        get_pv.set({"pvname": self.pvname})
        #get_pv.set({"control.limitLow": 3.45})  #  TODO  need data applicators for NON NT!!
        return get_pv # TODO rename to set_pv as only set PV uses this now

    def generate_pv_from_data(self, data):
        return_value = self.get_empty_pv()
        return_value.setValue(self.python_type(data[0]))
        unix_nanoseconds = generate_timestamp(data)
        apply_pv_timestamp(return_value, int(unix_nanoseconds * 1e9))
        apply_alarm_to_pv(return_value, data[1])
        return return_value


class arrayGenerator:
    def __init__(self, link):
        # self.epics_type = get_epics_type(link.secop_datatype.structure()["type"]) #innan
        self.epics_type = get_epics_type(link.datainfo.members_type.__class__)  # Type of array elements
        self.python_type = get_python_type(link.datainfo.members_type.__class__)
        self.meta_data_applicators = extract_pv_metadata(link)
        self.pv_name = link.get_reference
        self.pvname = link.set_reference  # For identifying set PV in write to topic

    def get_empty_pv(self):
        timestamp = pvaccess.PvTimeStamp(1, 1)  # Creating a pvapy timestamp structure to define the type in the PVobject
        alarm = pvaccess.PvAlarm(0, 0, "")  # Creating a pvapy alarm structure to define the type in the PVobject
        pv_conf = {
            'value': [self.epics_type],
            'descriptor': pvaccess.STRING,
            'timeStamp': timestamp,
            'alarm': alarm,
            "pvname": pvaccess.STRING
        }
        get_pv = pvaccess.PvObject(pv_conf)
        if self.pvname: #  TODO: better fix. Only links with set pv need "pvname". This should be solved elswhere
            get_pv.set({"pvname": self.pvname})
        #  Not testing below yet
        # apply_pv_metadata(self.meta_data_applicators, return_value)
        return get_pv #return_value

    def generate_pv_from_data(self, data):
        return_value = self.get_empty_pv()
        return_value['value'] = data[0]
        unix_nanoseconds = generate_timestamp(data)
        apply_pv_timestamp(return_value, int(unix_nanoseconds ))  # NOT an NT pv timestamp
        apply_alarm_to_pv(return_value, data[1])
        return return_value


class NtEnumGenerator:
    def __init__(self, link):
        self.epics_type = get_epics_type(link.datainfo.__class__)
        self.python_type = get_python_type(link.datainfo.__class__)
        self.meta_data_applicators = extract_pv_metadata(link)
        self.pv_name = link.get_reference
        self.pvname = link.set_reference  # For identifying set PV in "write to topic" function
        self.choices = link.datainfo.serialize()["members"]
        od = collections.OrderedDict(sorted(self.choices.items()))
        self.ordered_choices = []
        #  "Choices" in EPICS Enum probably need order
        for key in od:
            self.ordered_choices.append(key)


    def get_empty_pv(self):
        get_pv = pvaccess.NtEnum()
        get_pv['value.choices'] = self.ordered_choices
        apply_pv_metadata(self.meta_data_applicators, get_pv)
        return get_pv

    def generate_pv_from_data(self, data):
        return_value = self.get_empty_pv()
        return_value['value.index'] = data[0]
        unix_nanoseconds = generate_timestamp(data)
        apply_NTpv_timestamp(return_value, int(unix_nanoseconds))
        apply_alarm_to_pv(return_value, data[1])
        return return_value

class enumGenerator:
    def __init__(self, link):
        self.epics_type = get_epics_type(link.datainfo.__class__)
        self.python_type = get_python_type(link.datainfo.__class__)
        self.meta_data_applicators = extract_pv_metadata(link)
        self.pv_name = link.get_reference
        self.pvname = link.set_reference  # For identifying set PV in write to topic
        self.choices = link.datainfo.serialize()["members"]
        od = collections.OrderedDict(sorted(self.choices.items()))
        self.ordered_choices = []
        #  "Choices" in EPICS Enum probably need order
        for key in od:
            self.ordered_choices.append(key)

    def get_empty_pv(self):
        timestamp = pvaccess.PvTimeStamp(1, 1)  # Creating a pvapy timestamp structure to define the type in the PVobject
        alarm = pvaccess.PvAlarm(0, 0, "")  # Creating a pvapy alarm structure to define the type in the PVobject
        ntenum = pvaccess.NtEnum()
        pv_conf = {
            'value': ntenum.getValue(),
            'descriptor': pvaccess.STRING,
            'timeStamp': timestamp,
            'alarm': alarm,
            "pvname": pvaccess.STRING
        }
        set_pv = pvaccess.PvObject(pv_conf)
        set_pv['value.choices'] = self.ordered_choices
        if self.pvname: #  TODO: better fix. Only links with set pv need "pvname". This should be solved elswhere
            set_pv.set({"pvname": self.pvname})
        return set_pv

    def generate_pv_from_data(self, data):
        return_value = self.get_empty_pv()
        return_value['value.index'] = data[0]
        unix_nanoseconds = generate_timestamp(data)
        apply_NTpv_timestamp(return_value, int(unix_nanoseconds))
        apply_alarm_to_pv(return_value, data[1])
        return return_value


class structGenerator:
    def __init__(self, link):
        self.pv_name = link.get_reference
        self.pvname = link.set_reference  # For identifying set PV in write to topic
        self.datainfo = link.datainfo

    def get_empty_pv(self):
        timestamp = pvaccess.PvTimeStamp(1, 1)  # Creating a pvapy timestamp structure to define the type in the PVobject
        alarm = pvaccess.PvAlarm(0, 0, "")  # Creating a pvapy alarm structure to define the type in the PVobject
        structure = self.struct_to_pv_conf(self.datainfo)
        pv_conf = {
            'value': structure,
            'descriptor': pvaccess.STRING,
            'timeStamp': timestamp,
            'alarm': alarm,
            "pvname": pvaccess.STRING
        }
        get_pv = pvaccess.PvObject(pv_conf)
        if self.pvname: #  TODO: better fix. Only links with set pv need "pvname". This should be solved elswhere
            get_pv.set({"pvname": self.pvname})
        get_pv["pvname"] = self.pv_name

        #  Not testing below yet
        #apply_pv_metadata(self.meta_data_applicators, return_value)
        return get_pv

    def datainfo_to_pv_conf(self, datainfo):
        if datainfo.__class__ in SECOP_TO_EPICS_TYPES:
            return SECOP_TO_EPICS_TYPES[datainfo.__class__]
        elif isinstance(datainfo, SecStruct):
            return self.struct_to_pv_conf(datainfo)
        elif isinstance(datainfo, SecArray):
            return self.array_to_pv_conf(datainfo)
        elif isinstance(datainfo, SecTuple):
            return self.array_to_pv_conf(datainfo)
        else:
            raise TypeError(f"Unsupported SECoP data type in EPICS struct: {datainfo.member.__class__.__name__}")

    def struct_to_pv_conf(self, datainfo):
        conf = {}
        for key, member in datainfo.members.items():
            conf[key] = self.datainfo_to_pv_conf(member)
        return conf


    def array_to_pv_conf(self, datainfo):
        member_type = self.datainfo_to_pv_conf(datainfo.members)
        conf = [ member_type ]
        return conf


    def write_value(self, value):
        # Validate the value by setting source_value in the accessible
        self._accessible.source_value = value
        validated_value = self._accessible.source_value
        # Update PV timestamp
        #self.update_timestamp(validated_value)
        # Update the PV with new value
        self._pv.set(validated_value[0])


    def generate_pv_from_data(self, data):
        return_value = self.get_empty_pv()
        return_value['value'] = data[0]
        unix_nanoseconds = generate_timestamp(data)
        apply_pv_timestamp(return_value, int(unix_nanoseconds ))  # NOT an NT pv timestamp
        apply_alarm_to_pv(return_value, data[1])

        return return_value


class NtTupleGenerator:
    def __init__(self, link):
        self.column_epics_type_list = []
        self.column_python_type_list = []
        self.column_name_list = []
        # for name, value in link.secop_datatype.structure()["members"]["members"].items():
        for value in link.datainfo.members:
            self.column_epics_type_list.append(get_epics_type(value.type))
            self.column_python_type_list.append(get_python_type(value.type))
        self.meta_data_applicators = extract_pv_metadata(link)
        self.pv_name = link.get_reference

    def get_empty_pv(self):
        return_value = pvaccess.NtTable(self.column_epics_type_list)
        apply_pv_metadata(self.meta_data_applicators, return_value)
        return return_value

    def generate_pv_from_data(self, data):
        return_value = self.get_empty_pv()
        column_list = []
        for column_type in self.column_python_type_list:
            column_list.append(np.empty(len(data[0]), dtype=column_type))
        for i, row in enumerate(data[0]):
            for j, column_value in enumerate(list(row.values())):
                column_list[j][i] = self.column_python_type_list[j](column_value)
        for k, column_data in enumerate(column_list):
            return_value.setColumn(k, column_data.tolist())
        unix_nanoseconds = generate_timestamp(data)
        apply_NTpv_timestamp(return_value, unix_nanoseconds)
        apply_alarm_to_pv(return_value, data[1])
        return return_value


class NtTableGenerator:
    def __init__(self, link):
        self.column_epics_type_list = []
        self.column_python_type_list = []
        self.column_name_list = []
        # for name, value in link.secop_datatype.structure()["members"]["members"].items():
        for name, value in link.datainfo.members:
            self.column_epics_type_list.append(get_epics_type(value.type))
            self.column_python_type_list.append(get_python_type(value.type))
            self.column_name_list.append(name)
        self.meta_data_applicators = extract_pv_metadata(link)
        self.pv_name = link.get_reference

    def get_empty_pv(self):
        return_value = pvaccess.NtTable(self.column_epics_type_list)
        return_value.setLabels(self.column_name_list)
        apply_pv_metadata(self.meta_data_applicators, return_value)
        return return_value

    def generate_pv_from_data(self, data):
        return_value = self.get_empty_pv()
        column_list = []
        for column_type in self.column_python_type_list:
            column_list.append(np.empty(len(data[0]), dtype=column_type))
        for i, row in enumerate(data[0]):
            for j, column_value in enumerate(list(row.values())):
                column_list[j][i] = self.column_python_type_list[j](column_value)
        for k, column_data in enumerate(column_list):
            return_value.setColumn(k, column_data.tolist())
        unix_nanoseconds = generate_timestamp(data)
        apply_NTpv_timestamp(return_value, unix_nanoseconds)
        apply_alarm_to_pv(return_value, data[1])
        return return_value


def get_pv_generator(link):
    if isinstance(link.datainfo, SecArray):
        # Array
        members_type = link.datainfo.members_type
        if isinstance(members_type, SecArray) or \
           isinstance(members_type, SecStruct) or \
           isinstance(members_type, SecTuple):
            print("In get_pv_generator: NTtable")
            return NtTableGenerator(link)
        else:
            print("In get_pv_generator: Array")
            return arrayGenerator(link)
    # Struct
    elif isinstance(link.datainfo, SecEnum):
        print("In get_pv_generator: enum")
        return NtEnumGenerator(link)
    elif isinstance(link.datainfo, SecStruct):
        # Struct
        print("In get_pv_generator: struct")
        return structGenerator(link)
    elif isinstance(link.datainfo, SecTuple):
        # Tuple
        # FIXME: Add support for tuples
        print("In get_pv_generator: tuple")
        return NtTupleGenerator(link)
    else:
        # Scalar (all single-value types)
        print("In get_pv_generator: NT scalar")
        return NtScalarGenerator(link)


def set_pv_generator(link):  #  Needed as the set PVs need a "name" as a handle incoming PVs to match to link. TODO
   if link.secop_datatype.structure()["type"] == "array":
       if "members" in link.secop_datatype.structure()["members"]:
           print("In set_pv_generator: NTtable")
           return NtTableGenerator(link)
       else:
           return arrayGenerator(link)
   elif isinstance(link.datainfo, SecEnum):
       print("In set_pv_generator: enum")
       return enumGenerator(link)
   elif link.secop_datatype.structure()["type"] == "struct":
       print("In set_pv_generator: struct")
       return structGenerator(link)
   print("In set_pv_generator: scalar")
   return scalarGenerator(link)  #  for SET PVs the "pvname" must be used to identify incoming PVs (match to link)
