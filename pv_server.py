

from os import path
import sys
sys.path.insert(0, path.abspath(path.join(path.dirname(__file__), '../')))

import lib.links.links as links
import lib.links.structure_report_tools as linkstools
from lib.config_loader import config_loader
import lib.links.settings as settings
from lib.links.epics_link import EpicsLink
import numpy as np

config = config_loader.load("../config") # TODO added ../config, symlink shoudl be used i think!!
mqtt_settings = config.get('mqtt')
structure_topic = config['node manager']['topics']['structure']
#structure_topic= "210607meh/octopy/structure" #"ses/structure"  #For AP local testing.
import paho.mqtt.client as mqtt
import pvaccess
import sys
import time
import json

class PVAServer(object):
    def __init__(self):
        while True:
            try:
                self.server = pvaccess.PvaServer()
                break
            except RuntimeError as err:
                print(f"Failed to create pvAccess server with error message: {err}")
                print('Retrying in 10s')
                time.sleep(10)

        self.linklist = links.LinkList(link_type=EpicsLink)

    def extract_epics_datatype(self, lnk):
        """ Using a dict to find corresponding datatype to be used in epics. Looking at the typ defined (mandatory) in
             the links secop_structure"""
        datatype = {
            'short': pvaccess.INT,
            'int': pvaccess.LONG,
            'double': pvaccess.DOUBLE,
            'string': pvaccess.STRING,
            'bool': pvaccess.INT,  # TODO: ShaLL be pvaccess.BOOLEAN here! BUT CS Studio cannot handle BOOLEAN currently. JIRA Issue added.
            'enum': pvaccess.INT,
            'array': pvaccess.NtTable  #  TODO: Temporary solution. Will only work in specific cases
        }
        lnk.GW_datatype = datatype.get(lnk.secop_datatype.structure()["type"], None)
        if lnk.GW_datatype is None:
            #  TODO: What action if no secop datatype provided? Exit with error? Or as below, try to find old "datatype"
            lnk.GW_datatype = pvaccess.STRING
            print('No epics datatype, defaults to str....')
        return

    def get_epics_datatype(self, type): #  Translate types. Shoudl be consolidated with same function above

        datatype = {
            'short': pvaccess.INT,
            'int': pvaccess.LONG,
            'double': pvaccess.DOUBLE,
            'string': pvaccess.STRING,
            'bool': pvaccess.INT,  # TODO: ShaLL be pvaccess.BOOLEAN here! BUT CS Studio cannot handle BOOLEAN currently. JIRA Issue added.
            'enum': pvaccess.INT,
            'array': pvaccess.NtNdArray
            #'array': pvaccess.NtTable  #  TODO: Temporary solution. Will only work in specific cases (timestampcrate)
        }
        epicstype = datatype.get(type, None)
        if epicstype is None:
            epicstype = pvaccess.STRING

        return epicstype

    def extract_python_datatype(self, lnk):
        """ Using a dict to find corresponding datatype to be used in python. Looking at the typ defined (mandatory) in
             the links secop_structure"""
        datatype = {
            'int': int,
            'double': float,
            'string': str,
            'bool': bool,
            'array': list
        }
        lnk.python_datatype = datatype.get(lnk.secop_datatype.structure()["type"], None)
        if not lnk.python_datatype:
            print('No python datatype, defaults to str')
            lnk.python_datatype = str
        return

    def exract_dict_from_struct(self, dict):
        #print("Extracting a dict from datainfo struct ------------------------------")
        # At this point it is clear the incoming dict is a struct!
        dictatsionefinale = {}
        for k, member in dict.items():
            if member["type"] == "struct":
                dictatsionefinale[k] = self.exract_dict_from_struct(member["members"])
            elif member["type"] == "array":
                if member["members"]["type"] == "struct" or member["members"]["type"] == "array":
                    dictatsionefinale[k] = [self.exract_dict_from_struct(member["members"])]
                else:
                    dictatsionefinale[k] = [self.get_epics_datatype(member["members"]["type"])]

            else:
                dictatsionefinale[k] = self.get_epics_datatype(member["type"])

        struct_dict = dictatsionefinale

        return struct_dict


    def add_pv(self, lnks, write_to_topic_func):
        """
        Creates a PvObject for get_reference and set_reference if defined in the link. 1 PV is configured for each.
        The PV objects are then added to the pvaccess server as a record with a PV name. The object is also added to
        the link and can be accessed for update.
        Now NTTypes are used and it is not essential to add that to the PV (as before ts = pvaccess.PvTimeStamp(1, 1))
        :param lnks:
        :param write_to_topic_func:
        :return:
        """
        for lnk in lnks:
            # Passing the link object to functions adding (to the link) information on datatype in correct format
            self.extract_epics_datatype(lnk)
            self.extract_python_datatype(lnk)
            value_complete = {}  #  Dict to contain the PV structure. Is used both for get and set PV if required.
            ts = pvaccess.PvTimeStamp(1, 1)  # Creating a pvapy timestamp structure to define the type in the PVobject
            al = pvaccess.PvAlarm(0, 0, "")  # Creating a pvapy alarm structure to define the type in the PVobject
            try:
                #  TODO: First step to handle more complex types..here handling an array
                #  This currently handles scalar (not structs/arrays in arrays)  arrays in the structure
                if lnk.secop_datatype.structure()["type"] == 'struct':
                    value_dict = self.exract_dict_from_struct(lnk.secop_datatype.structure()["members"])
                    #  Adding the complete value struct and adding descriptor, alarm, timestamp fields
                    value_complete = {'value': value_dict, 'descriptor':pvaccess.STRING, 'timeStamp': ts,'alarm': al, "pvname": pvaccess.STRING}
                    get_pv = pvaccess.PvObject(value_complete)

                elif lnk.secop_datatype.structure()["type"] == 'array':
                    #  TODO This handles scalar arrays in NT tables. It cannot handle structs in arrays
                    keylist = []
                    valuelist = []
                    #  TODO: Here trying to handle arbritray number of columns in the array (but failed)
                    #  NOt even considering an array of arrays..
                    if "members" in lnk.secop_datatype.structure()["members"]: # -> Separate handling for  array of structs
                        # Handling the timestamping crate case separately now
                        for key, value in lnk.secop_datatype.structure()["members"]["members"].items():
                            keylist.append(key)
                            valuelist.append(self.get_epics_datatype(value["type"]))
                        get_pv = pvaccess.NtTable(valuelist)
                        get_pv.setLabels(keylist)
                    else: # For a scalar arrays.
                        # TODO just duplicating from above. to be improved
                        value_dict = [self.get_epics_datatype(lnk.secop_datatype.structure()["members"]["type"])] # self.exract_dict_from_struct(lnk.secop_datatype.structure()["members"])
                        value_complete = {'value': value_dict, 'descriptor': pvaccess.STRING, 'timeStamp': ts,
                                          'alarm': al, "pvname": pvaccess.STRING}
                        get_pv = pvaccess.PvObject(value_complete)


                else:
                    value_complete = {'value': lnk.GW_datatype, 'descriptor': pvaccess.STRING, 'timeStamp': ts,
                                          'alarm': al, "pvname": pvaccess.STRING}
                    get_pv = pvaccess.NtScalar(lnk.GW_datatype)

            except ValueError as err:
                print(err)
                return

            #  Adding the created pvObject to the Link object (this allows updating in callback)
            lnk.get_reference_pv_obj = get_pv
            if lnk.get_reference:
                #  Adding the getPV as a record, setting the PV name from link(get_reference)
                #  Should a get pv be used for "triggering" a read of the get reference(?...) Not implemented currently
                self.server.addRecord(lnk.get_reference, get_pv)
                # Add static data. TODO: Below shoudl be adapted to use the data in secop_structure!
                if hasattr(lnk, 'description'):
                    get_pv.setString('descriptor', str(lnk.description))
                # BELOW try just to avoid issues with new struct tests
                if not lnk.secop_datatype.structure()["type"] == 'array' : #  NTtable do not have below parameters
                    if 'unit' in lnk.secop_datatype.structure():
                        get_pv.getDisplay().setUnits(str(lnk.secop_datatype.structure()["unit"]))
                    if 'min' in lnk.secop_datatype.structure():
                        get_pv.getControl().setLimitLow(float(lnk.secop_datatype.structure()["min"]))
                    if 'max' in lnk.secop_datatype.structure():
                        get_pv.getControl().setLimitHigh(float(lnk.secop_datatype.structure()["max"]))

            else:
                print("There is no get reference in this link!")

            if lnk.set_reference: #  TODO: Add functionality to set array and structs..
                #  Creating a new PV object for the set_reference
                #  TODO: Unfortunately currently not solved how to use NTtypes! The problem is when a the callback
                #        function is triggered by an update ONLY the PVobject is recieved in the callback, an dcurrently
                #        I have not found a way to extract from which PV that originates!! So teh corresponding link can
                #        not be found! Temporary solution is to use non NTtype PVs and pass a "name"

                #  TODO Trying here to copy the get PV defined above. However, need to rethink on handlinonly Set PVs..
                set_pv = get_pv.copy()  # pvaccess.PvObject(value_complete)
                set_pv = pvaccess.PvObject(value_complete)
                #  TODO: Probably add datainfo metadata (min, max, unit) here also
                #set_pv["pvname"] = ''
                set_pv.set({"pvname": lnk.set_reference})  # TODO: Potentially essential? How to find correct link in write??
                #  Adding the created pvObject to the Link object
                lnk.set_reference_pv_obj = set_pv
                #  Adding the created PV as record to the server. Passing the function allowing publishing data on
                #  set topic. Triggers on channel write.
                self.server.addRecord(lnk.set_reference, set_pv, write_to_topic_func)
            else:
                print("No set reference provided in this link")

            if lnk.set_reference:
                print("Adding PV: set reference ->  {}  set topic->  {}".format(lnk.set_reference, lnk.set_topic))

            if lnk.get_reference:
                print("Adding PV: get reference ->  {}  get topic->  {}".format(lnk.get_reference, lnk.get_topic))

        return lnks  # Returns a link list of all listed links on this "server"

    def remove_pv(self, lnk):
        # self.server.removeRecord(lnk.set_reference)
        if lnk.set_reference:
            if self.server.hasRecord(lnk.set_reference):
                print("removing set ref PV")
                self.server.removeRecord(lnk.set_reference)
        if lnk.get_reference:
            if self.server.hasRecord(lnk.get_reference):
                print("removing get ref PV")
                self.server.removeRecord(lnk.get_reference)

    def write_to_pv(self, client, userdata, msg):
        """
            This function is triggered as callback function when a link topic (e.g. from HW side)have been updated.
            The corresponding PV is fetched from the linklist by the messages value_topic (that was updated).
            The data in the msg should follow SECoP datainfo format [data, {"t":timestamp}, ....]

            TODO: Need to add syntax checks and also handling of more types.
            """

        #m_decoded = msg.payload.decode('utf-8')
        message = json.loads(msg.payload.decode('utf-8'))
        #  Find the corresponding link from the topic in MQTT payload.
        #  Extracting the correct link using the "topic" as "key"
        print(msg.topic)
        lnk = self.linklist.find_by_value_topic(msg.topic)
        #  Transfer timestamp. Just a non verified dummy conversion used now..

        msg_data = None
        #  TODO: Need to add handling of structs
        #  Need to catch enum etc when extraction data. Implement SECoP syntax.
        if isinstance(message[0], dict) and "index" in message[0]:
            if "index" in message[0]:
                msg_data = lnk.python_datatype(message[0].get("index"))
        else:
            # TODO: Probably uneccacary below.. should be correct as it is python...?
            msg_data = message[0]#lnk.python_datatype(message[0])  # Extracting the data from the MQTT payload (and typecast)

        # Extract the PV object  from link
        pv = lnk.get_reference_pv_obj
        link_datatype = lnk.secop_datatype.structure()["type"]
        if link_datatype is not None:

            if link_datatype == "double":
                pv.setDouble('value', msg_data)
            elif link_datatype == "string":
                pv.setString('value', msg_data)

            elif link_datatype == "int":

                #TODO: The typecast is not the way to go either (teh link has an datatype set, but the data is string..)
                pv['value'] = msg_data#pv.setInt('value', msg_data)
            elif link_datatype == "enum":
                #  TODO: make better solution using structures
                pv.setInt('value', int(msg_data))

            elif link_datatype == "bool": #TODO: Below is temporar fix as CSS cannot handle BOOLEAN.
                #pv.setBoolean('value', msg_data)
                val = None
                if msg_data == True:
                    val = 1
                elif msg_data == False:
                    val=0
                else:
                    print("something not working")
                pv.setInt('value', val)

            elif link_datatype == "struct":
                #  TODO below just some hints ..to not forget..
                #pv.setStructure({'knob_1': {'knob_1_2': 34.5, 'knob_1_4': 39.001}})
                #pv.setStructure({'knob_4': 34.6})
                #pv['value.knob_5'] = 83.002
                #pv['value.knob_1.knob_1_4'] = 13.222
                #pv['value.knob_1'] = {'knob_1_4': 7.5, 'knob_1_2': 8.5}
                pv['value'] = msg_data

            elif link_datatype == "array":
                #  TODO: currently trying to handle NTtable array separately. Just checking if it has "members"
                #  if not, just try to handle it as an array
                if "members" in lnk.secop_datatype.structure()["members"]:  # if members -> array of structs -> NTtable
                    data = []
                    timestamps = []

                    for things in message[0]:
                        for item in things.items():
                            if item[0] == "t":
                                timestamps.append(item[1])
                            else:
                                data.append(item[1])

                    pv.setColumn(0, data)
                    pv.setColumn(1, timestamps)
                else: #  handling scalar array
                    pv['value'] = msg_data
            else:
                print ("Something went wrong when extracting datatype in writing to pv ")
        else:
            pv.setString('value', msg_data)
            print("Setting to string by default. something is wrong..)")

        #*********************************************************************************
        #  Just a first test to work with metadata. To be expanded
        message_datareport = message[1]  #  Message should have format [data, {"t":241753773, "key":value, , , }]
        if "alarm" in message_datareport.keys():
            pv.setStructure('alarm', {'message': message_datareport["alarm"] })
            print("Print out Alarm in PV structure:   {}    {}".format(message[1], message_datareport["alarm"]))

        if isinstance(message, list) and (len(message) > 2): #  TODO: How to make this check nice?
            unix_nanoseconds = message[1]["t"]
        else:
            unix_nanoseconds = round(time.time()*1000000000)

        timestamp = pvaccess.PvTimeStamp(unix_nanoseconds // 1000000000,
                                         (unix_nanoseconds - (unix_nanoseconds // 1000000000 * 1000000000)))

        #  TOCO Shoudl probably add a separate handling for teh NYTtable timestamp as a start
        #pv.setTimeStamp(timestamp) #  TODO i think this function is only available for NT table!
        pv['timeStamp'] = timestamp
        # TODO temporarily removed ts above. only works for NT table....?
        #  FInally Update the PV on the server with the pv object
        try:
            self.server.update(lnk.get_reference, pv)
        except (ValueError) as err:
            print('internal, write_to_pv: ' + str(err))

        print("Recieved:  {}     on topic:  {}     writing to PV:   {}".format(message, msg.topic, lnk.get_reference))


class LinkClient(object):
    """
    A MQTT client setup to listen to topics for adding (or removing) a new ECS EPICS link.
    The callback function triggered by publishing on the "/add-link" expects a list (?) of links in dict format as a Json.
    """
    def __init__(self,  mqtt_host=settings.OCTOPUS_DEFAULT_BROKER, mqtt_port=settings.DEFAULT_MQTT_TCP_PORT):

        self.topic = "pv_server_publishonThis" # TODO this is only published to inform some part of sw about created PVs...
        self.description = ""
        self.linklist = links.LinkList(link_type=EpicsLink)
        self.mqtt_host = mqtt_host
        self.mqtt_port = mqtt_port
        self.mqtt_client = mqtt.Client()
        auth = {'username': 'mesi', 'password': 'vaha23neca'}
        if auth:
            self.mqtt_client.username_pw_set(**auth)
        self.server = PVAServer()

        try:
            self.mqtt_client.on_connect = self.on_mqtt_connect
            self.mqtt_client.connect(self.mqtt_host, self.mqtt_port, 60)
            self.loops = 0
            self.mqtt_client.loop_start()  # Starts separate loop for handling MQTT calls
            while True:
                time.sleep(0.01)
                #  TODO: Align with other approaches. The iot_hwgw_gui_control.py uses the automatic .loop_start().
                if self.loops > 200:
                    # self.mqtt_client.publish('wd/' + self.topic, 'EPICS HWGW')  # TODO: for testing ink, fix!
                    self.expose_links_on_mqtt()  # For constant Update of GUI  TODO: Should this be here..
                    self.loops = 0
                else:
                    self.loops += 1

        except RuntimeError:
            self.cleanup('Runtime error')

        except KeyboardInterrupt:
            self.cleanup('Keyboard interrupt')

        self.cleanup('Normal')

    def cleanup(self, reason):
        print('Exiting: ' + reason)
        self.mqtt_client.loop_stop()
        self.mqtt_client.disconnect()
        sys.exit(0)

    def on_mqtt_connect(self, client, userdata, flags, rc):
        print("Connected with result code " + str(rc))
        self.mqtt_client.subscribe('#')
        self.mqtt_client.message_callback_add(self.topic + '/remove_link', self.on_mqtt_remove_link_message)
        #self.mqtt_client.message_callback_add("ses/structure/#", self.on_mqtt_add_describe_message)
        self.mqtt_client.message_callback_add(structure_topic, self.on_mqtt_add_describe_message)

    def on_mqtt_add_describe_message(self, client, userdata, config_msg):
        """Adding ECS links from complete describe message (structure report). As a describe message is recieved on the
             dedicated topic, it sent to links lib to generate a list of links. For each link a connection is made
             allowing monitor/subscribe on epics and mqtt sides"""
        print("Recieved a message on describe topic")

        describe_msg = config_msg.payload.decode('utf-8')
        describe = json.loads(describe_msg)
        print(describe)
        if describe["equipment_id"] == self.description: # Only one shall pass...for now...
            print("already running that secnode. Ignoring new describe")
            return
        else:
            self.description = describe["equipment_id"]
            validated_describe = linkstools.validate_structure(describe)
            ecs_linklist = linkstools.make_ecs_link_list(validated_describe)
            # Use topics for EPICS PV names. Requires replacing slash with colons. Also add set_reference, from topic
            for lnk in ecs_linklist:
                lnk.colonize()  # Exchanging / with : for PV names
            #  Adding all links to the servers linklist
            self.server.linklist = ecs_linklist
            self.create_pv_and_mqtt_callbacks(self.server.linklist)
            self.expose_links_on_mqtt()
            return

    def create_pv_and_mqtt_callbacks(self, ecs_linklist_dict):
        """ This function takes a list of ecs links as a dict.
         Each link in the list is check for duplicates (based on "value_topic") on server, any duplicates are removed.
         New links are added to server list
         PVs are created in the "add_link" server function
         Lastly MQTT callback functions are attached to the links to update PV when a MQTT topic is updated
         """

        #  Add PVs for links
        lnks = self.server.add_pv(ecs_linklist_dict, self.write_to_topic)

        for lnk in lnks:
            print("Link value topic: {}".format(lnk.value_topic))
            #  Adding callback function for updates on connected  MQTT topic. Topic updates triggers the MQTT
            #  callback function (in this case the write_to_pv funtion in the (PV) server), which updates the
            #  connected PV.
            self.mqtt_client.message_callback_add(lnk.value_topic, self.server.write_to_pv)
        self.print_summary()

    def print_summary(self):
        for link in self.linklist: #self.links:
            print("Created GetPV: {}  linked to topic:   {}".format(link.get_reference, link.get_topic))
            print("Created SetPV: {}  linked to topic:   {}".format(link.set_reference, link.set_topic))


    def write_to_topic(self, pv_msg):
        """Function used by Server class to publish updated PV data on correct MQTT topic defined in corresponding link.
        This function is added as argument to when adding pv- Retrieves correct link from linklist using "pvname"
        """

        lnk = self.server.linklist.find_by_set_reference(pv_msg["pvname"])

        """ The construction below is used to temporarily enable use of BOOLEANS in CS Studio which is currently not 
        possible. When using a BOOLEAN  CS Studio interprets it as a large structure/string. The main "value" is not 
         possible to acces , but it is possible to acces e.g. description or unit from PV/unit etc"""
        val = pv_msg['value']
        link_datatype = lnk.secop_datatype.structure()["type"]
        if link_datatype == "bool":
            if pv_msg["value"] == 1:
                val= True
            elif pv_msg["value"] == 0:
                val= False
            else:
                    print("something wrong with the bool fix")
        print("In write_to_topic. Recieved PV:  " + pv_msg["pvname"] + "  will be published on topic:  " +
              lnk.set_topic + "  with value:  " + str(pv_msg["value"]))
        #  TODO: Handling scalar arrays below! Need to implement struct handling etc
        if lnk.secop_datatype.structure()["type"] == "array": #  Handling scalar array-
            val = np.array(val).tolist()
        self.mqtt_client.publish(lnk.as_dict()['set topic'], json.dumps([val]).encode("utf-8"))

    def expose_links_on_mqtt(self):
        """

        Exposes (publishes) a list of available links on a topic available to a GUI
        :return:
        """

        self.mqtt_client.publish(self.topic + '/links', self.server.linklist.linklist_as_json(), retain=False)
        #  TODO: Test to expose links to the epics_ecsgw_controll. NEED new define path in "links" !!
        self.mqtt_client.publish("ecs_links", self.server.linklist.linklist_as_json(), retain=False)

    def remove_link(self, value_topic):
        lnk = self.server.linklist.find_by_value_topic(value_topic)
        if lnk:
            if lnk.set_topic:
                self.mqtt_client.message_callback_remove(lnk.set_topic)
            if lnk.get_topic:
                self.mqtt_client.message_callback_remove(lnk.get_topic)
            if lnk.value_topic:
                self.mqtt_client.message_callback_remove(lnk.value_topic)

            self.server.remove_pv(lnk)
            self.server.linklist.remove(lnk)

    def on_mqtt_remove_link_message(self, client, userdata, msg):
        self.remove_link(msg.payload.decode('utf-8'))
        self.expose_links_on_mqtt()


if __name__ == '__main__':
    lc = LinkClient()
    #  lc = LinkClient(topic="epics", mqtt_host="10.4.0.107")

