import sys
import time
import json
from os import path
import traceback
from lib.secop_data.secnode import SecNode
from lib.links.epics_sink_link import EpicsSinkLink
import logging

#sys.path.insert(0, path.abspath(path.join(path.dirname(__file__), '../')))

from lib.octopyapp.app import OctopyApp
import lib.links.links as links
import lib.links.structure_report_tools as linkstools
import lib.links.settings as settings
from lib.links.epics_link import EpicsLink
import numpy as np
from pva_server import PVAServer

APP_ID = 'EPICS ECS Gateway'
LOG_LEVEL = logging.DEBUG

LINK_PUBLISH_INTERVAL = 5

EPICS_PV_NAME_SEPARATOR = ":"

class EpicsECSGateway(OctopyApp):
    """
    A MQTT client setup to listen to topics for adding (or removing) a new ECS EPICS link.
    The callback function triggered by publishing on the "/add-link" expects a list (?) of links in dict format as a Json.
    """
    def __init__(self):
        super().__init__(APP_ID, LOG_LEVEL)

        #self.topic = "pv_server_publishonThis" # TODO this is only published to inform some part of sw about created PVs...
        #self.linklist = links.LinkList(link_type=EpicsLink)
        self.node = None
        self.links = []
        self.pva_server = PVAServer()


    def start(self):
        super().start(start_loop = False)
        self.subscribe(self.config['node manager']['topics']['structure'], self.on_mqtt_structure_message)
        # Start MQTT main loop in background thread
        self.mqtt_client.loop_start()
        self.main_loop()


    def main_loop(self):
        """Main MQTT loop
        The only thing it does is regularly publishing existing links.
        This serves no useful function other than for debugging.
        If this feature is no longer wanted the whole main_loop() method
        can be removed allowing the MQTT library handle everything itself
        """
        tick = 0.1  # 10 Hz refresh rate
        last_link_update = time.time() - LINK_PUBLISH_INTERVAL
        last_mqtt_misc = time.time()
        # MQTT main loop
        while True:
            try:
                self.mqtt_client.loop(tick)
                current_time = time.time()
                if current_time > last_link_update + LINK_PUBLISH_INTERVAL:
                    # Publish links over MQTT regularly
                    # This is not really needed and only used for debugging.
                    # TODO: Remove link publishing and the whole main_loop()
                    #       - let the MQTT library handle everything
                    last_link_update = current_time
                    self.publish_links()
                if current_time > last_mqtt_misc + 2:
                    # Let the MQTT library do its housekeeping regularly
                    last_mqtt_misc = current_time
                    self.mqtt_client.loop_misc()
            except RuntimeError:
                self.halt('Runtime error')
            except KeyboardInterrupt:
                self.halt('Keyboard interrupt')
            except BaseException as err:
                self.log(logging.ERROR, err)
                if LOG_LEVEL == logging.DEBUG:
                    traceback.print_exc()


    def halt(self, reason):
        self.log(logging.DEBUG, 'Exiting: ' + reason)
        # TODO: Clear link list and publish empty list. Provided that the
        #       link publishing feature is retained.
        self.mqtt_client.loop()
        self.stop()
        sys.exit(0)


    #def on_mqtt_connect(self, client, userdata, flags, rc):
    #    print("Connected with result code " + str(rc))
    #    self.mqtt_client.subscribe('#')
    #    self.mqtt_client.message_callback_add(self.topic + '/remove_link', self.on_mqtt_remove_link_message)
    #    self.mqtt_client.message_callback_add(structure_topic, self.on_mqtt_add_describe_message)



    def accessible_to_link(self, accessible):
        conf = {
            'value topic': accessible.value_topic,
            'set topic': accessible.set_topic,
            'get topic': accessible.get_topic,
            'datainfo': accessible.datainfo.serialize()
        }
        pv_name = accessible.path(separator = ':')
        if accessible.value_topic:
            conf['set reference'] = pv_name
        if accessible.set_topic:
            conf['get reference'] = pv_name
        link = EpicsSinkLink(conf)
        return link


    def on_mqtt_structure_message(self, client, userdata, message):
        prefix = message.topic.split('/')[0]
        self.log(logging.INFO, f"New structure message received: {prefix}")
        # FIXME: Check if node already exists. If it does unsubscribe to all topics and unload the node first.
        try:
            payload = json.loads(message.payload)
        except Exception as err:
            self.log(logging.ERROR, "Invalid JSON data encountered in SECoP structure report: " + str(err))
            return
        try:
            payload['id'] = payload['equipment_id']
            if self.node and payload['id'] == self.node.id:
                self.node.deserialize(payload)
                self.log(logging.INFO, f"Node updated: {self.node.full_name}")
            else:
                self.node = SecNode(payload)
                self.log(logging.INFO, f"New node: {self.node.full_name}")
        except Exception as err:
            self.log(logging.ERROR, "Error parsing SECoP structure: " + str(err))
            return
        self.log(logging.DEBUG, f"New node loaded with equipment id: {self.node.equipment_id}")
        # Remove all existing PV:s

        #if changes:
        #    for link in list(self.linklist):
        #        self.pva_server.remove_pv(link)
        #        self.linklist.delete(link)

        # Convert all accessibles in node to links
        all_accessibles = self.node.accessibles
        new_links = []
        for accessible in all_accessibles:
            link = self.accessible_to_link(accessible)
            new_links.append(link)
        self.links = new_links

        #  Adding all links to the servers linklist
        self.create_pv_and_mqtt_callbacks(self.links)
        self.publish_links()
        return


    def create_pv_and_mqtt_callbacks(self, links):
        """ This function takes a list of ecs links as a dict.
         Each link in the list is check for duplicates (based on "value_topic") on server, any duplicates are removed.
         New links are added to server list
         PVs are created in the "add_link" server function
         Lastly MQTT callback functions are attached to the links to update PV when a MQTT topic is updated
         """

        #  Add PVs for links
        for link in links:
            self.pva_server.add_pv(link, self.write_to_topic)
            print(f"Link value topic: {lnk.value_topic}")
            #  Adding callback function for updates on connected  MQTT topic. Topic updates triggers the MQTT
            #  callback function (in this case the write_to_pv funtion in the (PV) server), which updates the
            #  connected PV.
            self.mqtt_client.message_callback_add(link.value_topic, self.pva_server.write_to_pv)
        self.print_summary()


    def print_summary(self):
        for link in self.links:
            print("Created GetPV: {link.get_reference}  linked to topic:   {link.value_topic}")
            print("Created SetPV: {link.set_reference}  linked to topic:   {link.set_topic}")


    def write_to_topic(self, pv_msg):
        """Function used by Server class to publish updated PV data on correct MQTT topic defined in corresponding link.
        This function is added as argument to when adding pv- Retrieves correct link from linklist using "pvname"
        """

        lnk = self.pva_server.linklist.find_by_set_reference(pv_msg["pvname"])

        """ The construction below is used to temporarily enable use of BOOLEANS in CS Studio which is currently not 
        possible. When using a BOOLEAN  CS Studio interprets it as a large structure/string. The main "value" is not 
         possible to acces , but it is possible to acces e.g. description or unit from PV/unit etc"""
        val = pv_msg['value']
        link_datatype = lnk.secop_datatype.structure()["type"]
        if link_datatype == "bool":
            if pv_msg["value"] == 1:
                val= True
            elif pv_msg["value"] == 0:
                val= False
            else:
                    print("something wrong with the bool fix")
        #print("In write_to_topic. Recieved PV:  " + pv_msg["pvname"] + "  will be published on topic:  " +
        #      lnk.set_topic + "  with value:  " + str(pv_msg["value"]))
        #  TODO: Handling scalar arrays below! Need to implement struct handling etc
        if lnk.secop_datatype.structure()["type"] == "array": #  Handling scalar array-
            val = np.array(val).tolist()
        self.mqtt_client.publish(lnk.as_dict()['set topic'], json.dumps([val]).encode("utf-8"))

    def publish_links(self):
        return
        """

        Exposes (publishes) a list of available links on a topic available to a GUI
        :return:
        """

        self.mqtt_client.publish(self.topic + '/links', self.linklist.linklist_as_json(), retain=False)
        #  TODO: Test to expose links to the epics_ecsgw_controll. NEED new define path in "links" !!
        self.mqtt_client.publish("ecs_links", self.linklist.linklist_as_json(), retain=False)

    def remove_link(self, value_topic):
        lnk = self.linklist.find_by_value_topic(value_topic)
        if lnk:
            if lnk.set_topic:
                self.mqtt_client.message_callback_remove(lnk.set_topic)
            if lnk.get_topic:
                self.mqtt_client.message_callback_remove(lnk.get_topic)
            if lnk.value_topic:
                self.mqtt_client.message_callback_remove(lnk.value_topic)

            self.pva_server.remove_pv(lnk)
            self.linklist.remove(lnk)

    def on_mqtt_remove_link_message(self, client, userdata, msg):
        self.remove_link(msg.payload.decode('utf-8'))
        self.publish_links()


if __name__ == '__main__':
    gw = EpicsECSGateway()
    gw.start()
