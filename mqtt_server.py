import lib.links.links as links
import lib.links.structure_report_tools as linkstools
from lib.links.epics_ecs_link import EpicsECSLink as EpicsLink
from lib.config_loader import config_loader
import numpy as np
from pva_server import PVAServer
from lib.octopyapp import topics
import copy
import paho.mqtt.client as mqtt
import sys
import time
import json

config = config_loader.load("./config") # TODO added ../config, symlink shoudl be used i think!!
topics = topics.get_topics(config['mqtt']['prefix'])
structure_topic = topics['node manager']['structure']


class LinkClient(object):
    """
    A MQTT client setup to listen to topics for adding (or removing) a new ECS EPICS link.
    The callback function triggered by publishing on the "/add-link" expects a list (?) of links in dict format as a Json.
    """
    def __init__(self,  mqtt_host, mqtt_port):

        self.topic = "pv_server_publishonThis" # TODO this is only published to inform some part of sw about created PVs...
        self.description = ""
        self.linklist = links.LinkList(link_type=EpicsLink)
        self.mqtt_host = mqtt_host
        self.mqtt_port = mqtt_port
        self.mqtt_client = mqtt.Client()
        auth = {'username': 'mesi', 'password': 'vaha23neca'}
        if auth:
            self.mqtt_client.username_pw_set(**auth)
        self.server = PVAServer()

        try:
            self.mqtt_client.on_connect = self.on_mqtt_connect
            self.mqtt_client.connect(self.mqtt_host, self.mqtt_port, 60)
            self.loops = 0
            self.mqtt_client.loop_start()  # Starts separate loop for handling MQTT calls
            while True:
                time.sleep(0.01)
                #  TODO: Align with other approaches. The iot_hwgw_gui_control.py uses the automatic .loop_start().
                if self.loops > 200:
                    self.expose_links_on_mqtt()  # For constant Update of GUI  TODO: Should this be here..
                    self.loops = 0
                else:
                    self.loops += 1

        except RuntimeError:
            self.cleanup('Runtime error')

        except KeyboardInterrupt:
            self.cleanup('Keyboard interrupt')

        self.cleanup('Normal')

    def cleanup(self, reason):
        print('Exiting: ' + reason)
        self.mqtt_client.loop_stop()
        self.mqtt_client.disconnect()
        sys.exit(0)

    def on_mqtt_connect(self, client, userdata, flags, rc):
        print("Connected with result code " + str(rc))
        self.mqtt_client.subscribe('#')
        self.mqtt_client.message_callback_add(self.topic + '/remove_link', self.on_mqtt_remove_link_message)
        self.mqtt_client.message_callback_add(structure_topic, self.on_mqtt_add_describe_message)

    def on_mqtt_add_describe_message(self, client, userdata, config_msg):
        """Adding ECS links from complete describe message (structure report). As a describe message is recieved on the
             dedicated topic, it sent to links lib to generate a list of links. For each link a connection is made
             allowing monitor/subscribe on epics and mqtt sides"""

        describe_msg = config_msg.payload.decode('utf-8')
        describe = json.loads(describe_msg)
        print("Recieved a new message on describe topic")
        print(describe)
        #if describe["equipment_id"] == self.description: # Only one shall pass...for now...
        #  Above is obsolete. What is important here is the link list
        try:
            validated_describe = linkstools.validate_structure(describe)  #  TODO: Need to add code if fail ?
        except  Exception as err: #secop_errors.SecopException as err:#
            print(err)
            print("hejohåfel Cannot make ECS linklist out of this")
            return

        ecs_linklist = linkstools.make_ecs_link_list(validated_describe)
        #  Check if the received configuration is new or edited, if so remove the old and replace, if not then ignore
        changes = False
        if len(ecs_linklist) != len(self.linklist):
            print("Number of links changed")
            changes = True

        for link in ecs_linklist:
            if not (((self.linklist).find_by_value_topic(link.value_topic))):
                print("A topic in received configuration missing in current config ")
                changes = True
                break

            if link != (self.linklist).find_by_value_topic(link.value_topic):
                print("Topic Changes in link")
                changes = True
                break

        if changes:
            for link in list(self.linklist):
                self.server.remove_pv(link)
                self.linklist.delete(link)

            print("Setting up new or edited configuration")
            self.description = describe["equipment_id"]

            for lnk in ecs_linklist:
                lnk.colonize()  # Exchanging / with : for PV names

            #  Adding all links to the servers linklist
            self.linklist = copy.copy(ecs_linklist) #  linkstools.make_ecs_link_list(validated_describe)
            self.server.linklist = copy.copy(ecs_linklist) # linkstools.make_ecs_link_list(validated_describe)
            self.create_pv_and_mqtt_callbacks(self.linklist)
            self.expose_links_on_mqtt()
            return
        else:
            print("No changes in new configuration")
            return

    def create_pv_and_mqtt_callbacks(self, ecs_linklist_dict):
        """ This function takes a list of ecs links as a dict.
         Each link in the list is check for duplicates (based on "value_topic") on server, any duplicates are removed.
         New links are added to server list
         PVs are created in the "add_link" server function
         Lastly MQTT callback functions are attached to the links to update PV when a MQTT topic is updated
         """

        #  Add PVs for links
        lnks = self.server.add_pv(ecs_linklist_dict, self.write_to_topic)

        for lnk in lnks:
            print("Link value topic: {}".format(lnk.value_topic))
            #  Adding callback function for updates on connected  MQTT topic. Topic updates triggers the MQTT
            #  callback function (in this case the write_to_pv funtion in the (PV) server), which updates the
            #  connected PV.
            self.mqtt_client.message_callback_add(lnk.value_topic, self.server.write_to_pv)
        self.print_summary()

    def print_summary(self):
        for link in self.linklist:
            print("Created GetPV: {}  linked to topic:   {}".format(link.get_reference, link.get_topic))
            print("Created SetPV: {}  linked to topic:   {}".format(link.set_reference, link.set_topic))


    def write_to_topic(self, pv_msg):
        """Function used by Server class to publish updated PV data on correct MQTT topic defined in corresponding link.
        This function is added as argument to when adding pv- Retrieves correct link from linklist using "pvname"
        """
        print("INCOMING set PV now being written to !!!-----------")

        lnk = self.server.linklist.find_by_set_reference(pv_msg["pvname"])
        """ The construction below is used to temporarily enable use of BOOLEANS in CS Studio which is currently not 
        possible. When using a BOOLEAN  CS Studio interprets it as a large structure/string. The main "value" is not 
         possible to acces , but it is possible to acces e.g. description or unit from PV/unit etc"""
        val = pv_msg['value']
        link_datatype = lnk.secop_datatype.structure()["type"]
        if link_datatype == "bool":
            if pv_msg["value"] == 1:
                val= True
            elif pv_msg["value"] == 0:
                val= False
            else:
                    print("something wrong with the bool fix")
        elif link_datatype == "array": #  Handling scalar array-
            val = np.array(val).tolist()
        elif link_datatype == "enum":  # Handling enum
            val = val["index"]

        #print("In write_to_topic. Recieved PV:  " + pv_msg["pvname"] + "  will be published on topic:  " +
        #      lnk.set_topic + "  with value:  " + str(pv_msg["value"]))
        #  TODO: Below functions shoudl instead be functions in the PVgenerator! e.g. "generate data from PV"
        #  TODO: Handling scalar arrays below! Need to implement struct handling etc
        self.mqtt_client.publish(lnk.as_dict()['set topic'], json.dumps([val]).encode("utf-8"))

    def expose_links_on_mqtt(self):
        """

        Exposes (publishes) a list of available links on a topic available to a GUI
        :return:
        """

        self.mqtt_client.publish(self.topic + '/links', self.linklist.linklist_as_json(), retain=False)
        #  TODO: Test to expose links to the epics_ecsgw_controll. NEED new define path in "links" !!
        self.mqtt_client.publish("ecs_links", self.linklist.linklist_as_json(), retain=False)

    def remove_link(self, value_topic):
        lnk = self.linklist.find_by_value_topic(value_topic)
        if lnk:
            if lnk.set_topic:
                self.mqtt_client.message_callback_remove(lnk.set_topic)
            if lnk.get_topic:
                self.mqtt_client.message_callback_remove(lnk.get_topic)
            if lnk.value_topic:
                self.mqtt_client.message_callback_remove(lnk.value_topic)

            self.server.remove_pv(lnk)
            self.linklist.remove(lnk)

    def on_mqtt_remove_link_message(self, client, userdata, msg):
        self.remove_link(msg.payload.decode('utf-8'))
        self.expose_links_on_mqtt()


if __name__ == '__main__':
    lc = LinkClient(config['mqtt']['host'], config['mqtt']['port'])
    #  lc = LinkClient(topic="epics", mqtt_host="10.4.0.107")
